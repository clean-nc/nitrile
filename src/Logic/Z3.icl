implementation module Logic.Z3

/**
 * Copyright 2021-2022 the authors (see README.md).
 *
 * This file is part of Nitrile.
 *
 * Nitrile is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Nitrile is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Nitrile. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

import Control.Applicative
import Control.Monad
import Control.Monad.Fail
import Data.Error
import Data.Func
import Data.Functor
from Data.Map import :: Map
import qualified Data.Map
import Data.Tuple
import StdEnv

import Logic._Z3

:: Z3State = !
	{ ctx :: !Z3Context
	, sol :: !Z3Solver
	, opt :: !Z3Optimize
	}

:: Z3Result a = {r :: !a, st :: !Z3State}

// NB: we do not use StateT because we need strictness in the result.
:: Z3 a =: Z3 (Z3State -> MaybeError String (Z3Result a))

instance Functor Z3
where
	fmap f (Z3 m) = Z3
		\st -> case m st of
			Error e -> Error e
			Ok res=:{r} -> Ok {res & r=f r}

instance pure Z3 where pure x = Z3 \st -> Ok {r=x, st=st}
instance <*> Z3 where (<*>) f x = f >>= \f -> f <$> x

instance Monad Z3
where
	bind (Z3 x) f = Z3
		\st -> case x st of
			Error e -> Error e
			Ok {r,st} -> let (Z3 f`) = f r in f` st

instance MonadFail Z3
where
	fail s = Z3 \_ -> Error s

evalZ3 :: !(Z3 a) -> MaybeError String a
evalZ3 f =
	(\r -> r.r) <$> f`
		{ ctx = noContext
		, sol = noSolver
		, opt = noOptimize
		}
where
	(Z3 f`) = init >>| f >>= \x -> teardown >>| pure x

	init = Z3 \st
		| not Z3_global_param_reset_all -> abort "cannot happen\n"
		| not (Z3_global_param_set "sat.core.minimize" "true") -> abort "cannot happen\n"
		| not (Z3_global_param_set "smt.core.minimize" "true") -> abort "cannot happen\n"
		# cfg = Z3_mk_config
		# cfg = Z3_set_param_value "model" "true" cfg
		# cfg = Z3_set_param_value "proof" "true" cfg // needed for unsat cores
		# ctx = Z3_mk_context cfg
		# ctx = Z3_del_config cfg ctx
		-> Ok {r=(), st={st & ctx=ctx}}
	teardown = Z3 \st=:{ctx}
		| not (Z3_del_context ctx) -> Error "failed to delete Z3 context"
		| otherwise -> Ok {r=(), st={st & ctx=noContext}}

modify :: !(Z3State -> Z3State) -> Z3 ()
modify f = Z3 \st -> Ok {r=(), st=f st}

withContext :: !(Z3Context -> a) -> Z3 a
withContext f = Z3 \st -> Ok {r=f st.ctx, st=st}

withContext` :: !(Z3Context -> (a, Z3Context)) -> Z3 a
withContext` f = Z3 \st -> let (x,ctx) = f st.ctx in Ok {r=x, st={st & ctx=ctx}}

withSolverOrOptimizerAndContext :: !(Z3Solver Z3Context -> a) !(Z3Optimize Z3Context -> a) -> Z3 a
withSolverOrOptimizerAndContext f g = Z3
	\st
		| isNoSolver st.sol
			| isNoOptimize st.opt -> Error "withSolverOrOptimizerAndContext used outside withSolver / withOptimizer"
			| otherwise -> Ok {r=g st.opt st.ctx, st=st}
		| otherwise -> Ok {r=f st.sol st.ctx, st=st}

withSolverOrOptimizerAndContext`` :: !(Z3Solver Z3Context -> Z3Context) !(Z3Optimize Z3Context -> Z3Context) -> Z3 ()
withSolverOrOptimizerAndContext`` f g = Z3
	\st
		| isNoSolver st.sol
			| isNoOptimize st.opt -> Error "withSolverOrOptimizerAndContext`` used outside withSolver / withOptimizer"
			| otherwise -> Ok {r=(), st={st & ctx=g st.opt st.ctx}}
		| otherwise -> Ok {r=(), st={st & ctx=f st.sol st.ctx}}

withOptimizerAndContext`` :: !(Z3Optimize Z3Context -> Z3Context) -> Z3 ()
withOptimizerAndContext`` f = Z3
	\st
		| isNoOptimize st.opt -> Error "withOptimizerAndContext`` used outside withOptimizer"
		| otherwise -> Ok {r=(), st={st & ctx=f st.opt st.ctx}}

withSolver :: !(Z3 a) -> Z3 a
withSolver f =
	Z3 (\st -> Ok {r=(), st={st & sol=Z3_mk_solver st.ctx}}) >>|
	f >>= \x ->
	Z3 (\st -> Ok {r=(), st={st & sol=noSolver, ctx=Z3_solver_dec_ref st.sol st.ctx}}) >>|
	pure x

withOptimizer :: !(Z3 a) -> Z3 a
withOptimizer f =
	Z3 (\st -> Ok {r=(), st={st & opt=Z3_mk_optimize st.ctx}}) >>|
	f >>= \x ->
	Z3 (\st -> Ok {r=(), st={st & opt=noOptimize, ctx=Z3_optimize_dec_ref st.opt st.ctx}}) >>|
	pure x

disallowInSolverOrOptimizer :: !String -> Z3 ()
disallowInSolverOrOptimizer f = Z3
	\st
		| isNoSolver st.sol -> Ok {r=(), st=st}
		| otherwise -> Error (f +++ " cannot be used in withSolver")

mustBeInSolverOrOptimizer :: !String -> Z3 ()
mustBeInSolverOrOptimizer f = Z3
	\st
		| isNoSolver st.sol && isNoOptimize st.opt ->
			Error (f +++ " must be used in withSolver or withOptimizer")
		| otherwise ->
			Ok {r=(), st=st}

mustBeInOptimizer :: !String -> Z3 ()
mustBeInOptimizer f = Z3
	\st
		| isNoOptimize st.opt ->
			Error (f +++ " must be used in withOptimizer")
		| otherwise ->
			Ok {r=(), st=st}

escapeName :: !String -> String
escapeName s = {c \\ c <- escape [c \\ c <-: s]}
where
	escape [] = []
	escape [c:cs]
		| isAlphanum c || c == '.' || c == '-' = [c:escape cs]
		| otherwise = ['_',toHexDigit (toInt c >> 4),toHexDigit (toInt c bitand 0xf):escape cs]
	where
		toHexDigit i = if (i < 10) (toChar i + '0') (toChar (i-10) + 'a')

unescapeName :: !String -> String
unescapeName s = {c \\ c <- unescape [c \\ c <-: s]}
where
	unescape [] = []
	unescape ['_',x,y:cs] = [toChar ((fromHexDigit x << 4) + fromHexDigit y):unescape cs]
	where
		fromHexDigit c = if (c < 'A') (toInt (c - '0')) (toInt (c - 'a') + 10)
	unescape [c:cs] = [c:unescape cs]

makeExpr :: !Z3Expr -> Z3 Z3Ast
makeExpr e = withContext (make e)
where
	make :: !Z3Expr !Z3Context -> Z3Ast
	make Z3True ctx = Z3_mk_true ctx
	make Z3False ctx = Z3_mk_false ctx
	make (Z3Int i) ctx = Z3_mk_int i (Z3_mk_int_sort ctx) ctx
	make (Z3Eq x y) ctx = Z3_mk_eq (make x ctx) (make y ctx) ctx
	make (Z3Ite x y z) ctx = Z3_mk_ite (make x ctx) (make y ctx) (make z ctx) ctx
	make (Z3And terms) ctx = Z3_mk_and (map (flip make ctx) terms) ctx
	make (Z3Or terms) ctx = Z3_mk_or (map (flip make ctx) terms) ctx
	make (Z3App f args) ctx = Z3_mk_app f [make a ctx \\ a <- args] ctx

astToExpr :: !Z3Ast -> Z3 Z3Expr
astToExpr ast =
	withContext (Z3_get_ast_kind ast) >>= \kind ->
	case kind of
		?None ->
			fail "unknown ast kind in astToExpr"
		?Just Z3NumeralAst ->
			fail "numeral in astToExpr"
		?Just Z3AppAst ->
			makeAppExpr ast
		?Just Z3VarAst ->
			fail "var in astToExpr"
		?Just Z3QuantifierAst ->
			fail "quantifier in astToExpr"
		?Just Z3SortAst ->
			fail "sort in astToExpr"
where
	makeAppExpr ast =
		withContext (Z3_to_app ast) >>= \app ->
		withContext (Z3_get_app_decl app) >>= \decl ->
		withContext (Z3_get_app_num_args app) >>= \nargs ->
		mapM (\i -> withContext (Z3_get_app_arg i app)) [0..nargs-1] >>= \args ->
		Z3App decl <$> mapM astToExpr args

assert :: !Z3Expr -> Z3 ()
assert e =
	makeExpr e >>= \e ->
	withSolverOrOptimizerAndContext`` (Z3_solver_assert e) (Z3_optimize_assert e)

check :: Z3 Z3LBool
check = withSolverOrOptimizerAndContext Z3_solver_check (Z3_optimize_check [])

checkWithAssumptions :: ![Z3Expr] -> Z3 (Z3LBool, [Int])
checkWithAssumptions assumptions =
	mapM makeExpr assumptions >>= \assumptions ->
	withSolverOrOptimizerAndContext (Z3_solver_check_assumptions assumptions) (Z3_optimize_check assumptions) >>= \result ->
	case result of
		Z3LFalse ->
			withSolverOrOptimizerAndContext Z3_solver_get_unsat_core Z3_optimize_get_unsat_core >>= \asts_vec ->
			withContext (Z3_ast_vector_size asts_vec) >>= \n_asts ->
			mapM (\i -> withContext (Z3_ast_vector_get asts_vec i)) [0..n_asts-1] >>= \asts ->
			withContext (Z3_ast_vector_dec_ref asts_vec) >>|
			withContext (\ctx -> [i \\ unsat <- asts, i <- [0..] & org <- assumptions | Z3_is_eq_ast org unsat ctx]) >>= \unsatIds ->
			pure (Z3LFalse, unsatIds)
		result ->
			pure (result, [])

getReasonForUndef :: Z3 String
getReasonForUndef =
	withSolverOrOptimizerAndContext Z3_solver_get_reason_unknown Z3_optimize_get_reason_unknown

maximize :: !Z3Expr -> Z3 ()
maximize e =
	mustBeInOptimizer "maximize" >>|
	makeExpr e >>= \e ->
	withOptimizerAndContext`` (Z3_optimize_maximize e)

minimize :: !Z3Expr -> Z3 ()
minimize e =
	mustBeInOptimizer "minimize" >>|
	makeExpr e >>= \e ->
	withOptimizerAndContext`` (Z3_optimize_minimize e)

makeSymbol :: !String -> Z3 Z3Symbol
makeSymbol ident = withContext (Z3_mk_string_symbol (escapeName ident))

makeEnum :: !String ![String] -> Z3 (Z3Sort, [(Z3FuncDecl, Z3FuncDecl)])
makeEnum name conses =
	// Using Z3_query_constructor after Z3_mk_solver causes a segfault further down in the solver
	disallowInSolverOrOptimizer "makeEnum" >>|
	makeSymbol name >>= \name ->
	mapM makeConstructor conses >>= \constructors ->
	withContext (Z3_mk_datatype name constructors) >>= \sort ->
	mapM getFunDecls constructors >>= \funs ->
	pure (sort, funs)
where
	makeConstructor name =
		makeSymbol name >>= \cons ->
		makeSymbol ("is"+++name) >>= \recognizer ->
		withContext (Z3_mk_constant_constructor cons recognizer)
	getFunDecls constructor =
		withContext (Z3_query_constant_constructor constructor) >>= \(cons,recognizer) ->
		withContext` (\ctx -> ((), Z3_del_constructor constructor ctx)) >>|
		pure (cons, recognizer)

makeConst :: !String !Z3Sort -> Z3 Z3FuncDecl
makeConst name sort =
	// Not sure if it would cause problems, but makeConst in a prover is probably not useful
	disallowInSolverOrOptimizer "makeConst" >>|
	makeSymbol name >>= \name ->
	withContext (Z3_mk_func_decl name [] sort)

eqFuncDecl :: !Z3FuncDecl !Z3FuncDecl -> Z3 Bool
eqFuncDecl f1 f2 = withContext \ctx ->
	Z3_get_func_decl_id f1 ctx == Z3_get_func_decl_id f2 ctx

getModel :: Z3 Z3Model
getModel =
	mustBeInSolverOrOptimizer "getModel" >>|
	withSolverOrOptimizerAndContext Z3_solver_get_model Z3_optimize_get_model  >>= \model ->
	withContext (Z3_model_get_num_consts model) >>= \i ->
	'Data.Map'.fromList <$> mapM (flip getConst model) [0..i-1]
where
	getConst :: !Int !Z3Model` -> Z3 (String, Z3Expr)
	getConst i m =
		withContext (Z3_model_get_const_decl i m) >>= \const ->
		withContext (Z3_get_decl_name const) >>= \sym ->
		(unescapeName <$> withContext (Z3_get_symbol_string sym)) >>= \sym ->
		makeExpr (Z3App const []) >>= \constAst ->
		withContext (Z3_model_eval True constAst m) >>= \mbResultAst ->
		case mbResultAst of
			?None -> fail ("could not evaluate " +++ sym)
			?Just ast -> tuple sym <$> astToExpr ast
