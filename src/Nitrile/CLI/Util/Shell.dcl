definition module Nitrile.CLI.Util.Shell

/**
 * Copyright 2021-2023 the authors (see README.md).
 *
 * This file is part of Nitrile.
 *
 * Nitrile is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Nitrile is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Nitrile. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

from Data.Error import :: MaybeError
from System.FilePath import :: FilePath

/**
 * Run a shell script.
 * @param Callback for output on stdout.
 * @param Callback for output on stderr.
 * @param Callback for the exit code.
 * @param The lines of the script to run.
 * @param The command-line arguments for the script.
 * @param A temporary directory in which the script can be stored.
 * @param The initial state for the callbacks.
 */
runShellScript ::
	!(String .st *World -> (.st, *World))
	!(String .st *World -> (.st, *World))
	!(Int .st *World -> (.st, *World))
	![String] ![String] !FilePath
	!.st !*World -> (!MaybeError String .st, !*World)
