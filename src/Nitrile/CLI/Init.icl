implementation module Nitrile.CLI.Init

/**
 * Copyright 2021-2022 the authors (see README.md).
 *
 * This file is part of Nitrile.
 *
 * Nitrile is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Nitrile is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Nitrile. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

import Data.Error
import Data.Func
import Data.Functor
import SPDX.License
import StdEnv
import StdMaybe
import System.Directory
import System.File
import System.FilePath
import Text

import Data.SemVer
import Nitrile.CLI.Util
import Nitrile.Constants

init :: !*World -> (!Bool, !*World)
init w
	# (exi,w) = fileExists PACKAGE_FILE w
	| exi = fail (PACKAGE_FILE +++ " already exists.") w
	# (mbDir,w) = getCurrentDirectory w
	  dir = dropDirectory (fromOk mbDir)
	| isError mbDir = fail ("Failed to get current directory: " +++ snd (fromError mbDir)) w
	# (io,w) = stdio w
	# (name,io) = enterInfo "package name" dir ?None io
	# (description,io) = enterInfo "description" "" ?None io
	# (type,io) = enterInfo "package type" "Application"
		(?Just \t -> if (isMember t ["Application", "Library", "Miscellaneous"])
			(Ok ()) (Error "choose from Application, Library, or Miscellaneous")) io
	# (version,io) = enterInfo "version" "0.1.0" (?Just parseVersion) io
	# (license,io) = enterInfo "license" ""
		(?Just \l -> if (size l == 0) (Ok ()) (const () <$> mb2error "invalid SPDX license identifier" (fromLicenseId l)))
		io
	# (gitlab,io) = enterInfo "GitLab namespace" ""
		(?Just \a -> if (size a <> 0 && indexOf "/" a < 0) (Error "must contain a slash") (Ok ()))
		io
	# (maintainer,io) = enterInfo "maintainer" "" ?None io
	# (contact,io) = enterInfo "contact email" "" ?None io
	# (_,w) = fclose io w
	# yaml = join "\n" $ catMaybes
		[ ?Just $ "format_version: 0.4.4"
		, ?Just $ "name: " +++ name
		, if (size description == 0) ?None (?Just $ "description: " +++ description)
		, ?Just $ "type: " +++ type
		, ?Just $ "version: " +++ version
		, if (size license == 0) ?None (?Just $ "license: " +++ license)
		, if (size gitlab == 0) ?None (?Just $ "url: https://gitlab.com/" +++ gitlab)
		, if (size maintainer == 0) ?None (?Just $ "maintainer: " +++ maintainer)
		, if (size contact == 0) ?None (?Just $ "contact_email: " +++ contact)
		]
	# (mbError,w) = writeFile PACKAGE_FILE (yaml +++ "\n") w
	= succeed (?Just (concat3 "Wrote settings to " PACKAGE_FILE ".")) w
where
	enterInfo :: !String !String !(?(String -> MaybeError String a)) !*File -> (!String, !*File)
	enterInfo what default check io
		# io = io <<< "Enter " <<< what <<< " [" <<< default <<< "]: "
		# (answer,io) = freadline io
		# answer = trim answer
		# answer = if (size answer == 0) default answer
		= case check of
			?None -> (answer, io)
			?Just checkf -> case checkf answer of
				Error e
					# io = io <<< "Invalid " <<< what <<< ": " <<< e <<< "\n"
					-> enterInfo what default check io
				_
					-> (answer, io)
