implementation module Nitrile.Target

/**
 * Copyright 2021-2022 the authors (see README.md).
 *
 * This file is part of Nitrile.
 *
 * Nitrile is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Nitrile is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Nitrile. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

import Control.Monad
import Data.Error
import Data.GenEq
import Data.GenLexOrd
import StdEnv
import System.OS
import System._Architecture
from Text import class Text(split), instance Text String, concat3

CURRENT_PLATFORM :== IF_LINUX Linux (IF_MAC Mac (IF_WINDOWS Windows (abort "unknown platform\n")))
CURRENT_ARCH_FAMILY :== IF_INTEL Intel (IF_ARM ARM (abort "unknown architecture\n"))
CURRENT_BITWIDTH :== IF_INT_64_OR_32 BW64 BW32

derive gLexOrd Architecture, ArchitectureFamily, Bitwidth, Platform, Target
instance < Target where (<) x y = (x =?= y)=:LT

derive gEq Architecture, ArchitectureFamily, Bitwidth, Platform, Target
instance == Target where (==) x y = x === y

instance toString Target
where
	toString t = concat3 (toString t.platform) "-" (toString t.architecture)

instance toString Platform
where
	toString AnyPlatform = "any"
	toString Linux = "linux"
	toString Mac = "mac"
	toString Windows = "windows"

instance toString Architecture
where
	toString {family,bitwidth} = case family of
		AnyArchitectureFamily -> case bitwidth of
			AnyBitwidth -> "any"
			BW64 -> "64bit"
			BW32 -> "32bit"
		Intel -> case bitwidth of
			AnyBitwidth -> "intel"
			BW64 -> "x64"
			BW32 -> "x86"
		ARM -> case bitwidth of
			AnyBitwidth -> "arm"
			BW64 -> "arm64"
			BW32 -> abort "arm32 is not supported\n"

targetMatches :: !Target !Target -> Bool
targetMatches x y =
	platformMatches x.platform y.platform &&
	architectureMatches x.architecture y.architecture

platformMatches :: !Platform !Platform -> Bool
platformMatches AnyPlatform _ = True
platformMatches _ AnyPlatform = True
platformMatches Linux x = x=:Linux
platformMatches Mac x = x=:Mac
platformMatches Windows x = x=:Windows

architectureMatches :: !Architecture !Architecture -> Bool
architectureMatches x y =
	architectureFamilyMatches x.family y.family &&
	bitwidthMatches x.bitwidth y.bitwidth

architectureFamilyMatches :: !ArchitectureFamily !ArchitectureFamily -> Bool
architectureFamilyMatches AnyArchitectureFamily _ = True
architectureFamilyMatches _ AnyArchitectureFamily = True
architectureFamilyMatches Intel x = x=:Intel
architectureFamilyMatches ARM x = x=:ARM

bitwidthMatches :: !Bitwidth !Bitwidth -> Bool
bitwidthMatches AnyBitwidth _ = True
bitwidthMatches _ AnyBitwidth = True
bitwidthMatches BW32 x = x=:BW32
bitwidthMatches BW64 x = x=:BW64

currentTarget :: Target
currentTarget =
	{ platform = CURRENT_PLATFORM
	, architecture =
		{ family = CURRENT_ARCH_FAMILY
		, bitwidth = CURRENT_BITWIDTH
		}
	}

allTargets :: [Target]
allTargets = expandAnyInTarget {platform=AnyPlatform, architecture={family=AnyArchitectureFamily, bitwidth=AnyBitwidth}}
where
	expandAnyInTarget :: !Target -> [Target]
	expandAnyInTarget t =
		[ target
		\\ p <- allPlatforms t.platform
		, f <- allArchitectureFamilies t.architecture.family
		, bw <- allBitwidths t.architecture.bitwidth
		, let target = {platform=p, architecture={family=f, bitwidth=bw}}
		| isValidTarget target
		]
	where
		allPlatforms AnyPlatform = [Linux, Mac, Windows]
		allPlatforms p = [p]

		allArchitectureFamilies AnyArchitectureFamily = [Intel, ARM]
		allArchitectureFamilies a = [a]

		allBitwidths AnyBitwidth = [BW64, BW32]
		allBitwidths bw = [bw]

isValidTarget :: !Target -> Bool
isValidTarget {platform,architecture={family=ARM,bitwidth}} =
	platformMatches Linux platform &&
	bitwidthMatches BW64 bitwidth
isValidTarget {platform=Mac,architecture={bitwidth}} =
	bitwidthMatches BW64 bitwidth
isValidTarget _ =
	True

parseTarget :: !String -> MaybeError String Target
parseTarget s = case split "-" s of
	[platform, arch] ->
		liftM2
			(\platform arch -> {platform=platform, architecture=arch})
			(parsePlatform platform)
			(parseArchitecture arch)
	_ ->
		Error "target should have the form <PLATFORM>-<ARCHITECTURE>"

parsePlatform :: !String -> MaybeError String Platform
parsePlatform "any" = Ok AnyPlatform
parsePlatform "linux" = Ok Linux
parsePlatform "mac" = Ok Mac
parsePlatform "windows" = Ok Windows
parsePlatform s = Error (concat3 "unknown platform '" s "'")

parseArchitecture :: !String -> MaybeError String Architecture
parseArchitecture "any" = Ok {family=AnyArchitectureFamily, bitwidth=AnyBitwidth}
parseArchitecture "intel" = Ok {family=Intel, bitwidth=AnyBitwidth}
parseArchitecture "arm" = Ok {family=ARM, bitwidth=AnyBitwidth}
parseArchitecture "64bit" = Ok {family=AnyArchitectureFamily, bitwidth=BW64}
parseArchitecture "32bit" = Ok {family=AnyArchitectureFamily, bitwidth=BW32}
parseArchitecture "x64" = Ok {family=Intel, bitwidth=BW64}
parseArchitecture "x86" = Ok {family=Intel, bitwidth=BW32}
parseArchitecture "arm64" = Ok {family=ARM, bitwidth=BW64}
parseArchitecture arch = Error (concat3 "unknown architecture '" arch "'")
