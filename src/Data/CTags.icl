implementation module Data.CTags

/**
 * Copyright 2017-2022 the authors (see README.md).
 *
 * This file is part of Nitrile.
 *
 * Nitrile is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Nitrile is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Nitrile. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

import Data.List
import StdEnv
import StdMaybe
import Text

:: TagSet :== [Tag] // Always sorted

toTagSet :: ![Tag] -> TagSet
toTagSet tags = sort tags

tagSetSize :: !TagSet -> Int
tagSetSize tags = length tags

combineTagSets :: ![TagSet] -> TagSet
combineTagSets sets = foldr merge [] sets

removeComments :: !TagSet -> TagSet
removeComments tags = [{t & comments=[]} \\ t <- tags]

setComment :: !Comment !Tag -> Tag
setComment c tag = setComments [c] tag

setComments :: ![Comment] !Tag -> Tag
setComments cs t =
	{ t
	& comments = filter (\(c,_) -> isNone (lookup c cs)) t.comments ++ cs
	}

instance toString Tag
where
	toString tag =
		tag.ident +++ "\t" +++
		tag.file +++ "\t" +++
		toString tag.address +++ case tag.comments of
			[] -> ""
			cs -> foldl (\c (k,v) -> c + "\t" + k + ":" + v) ";\"" cs

instance toString TagAddress
where
	toString (Line i) = toString i
	toString (Search s) = "/" +++ s +++ "/"
	toString (CompoundAddress as) = join "," (map toString as)

instance <<< Tag where (<<<) f t = f <<< toString t <<< "\r\n"
instance <<< TagSet where (<<<) f ts = foldl (<<<) f ts

instance < Tag where (<) t1 t2 = t1.ident < t2.ident
