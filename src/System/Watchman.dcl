definition module System.Watchman

/**
 * Copyright 2022 the authors (see README.md).
 *
 * This file is part of Nitrile.
 *
 * Nitrile is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Nitrile is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Nitrile. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

from Data.Error import :: MaybeError
from System.FilePath import :: FilePath
from System.Process import :: ProcessHandle, :: ProcessIO

:: Watchman =
	{ processHandle :: !ProcessHandle
	, processIO     :: !ProcessIO
	, buffer        :: !String
	, responses     :: ![Response]
	}

/**
 * See https://facebook.github.io/watchman/docs/expr/allof.html and other
 * pages.
 * This data type is not complete.
 */
:: Expression
	= AllOf ![Expression]
	| AnyOf ![Expression]
	| Dirname !String !(?IntConstraint) //* Second argument is depth.
	| Not !Expression
	| Suffix !String
	| FileType !FileType
	| Wholename ![String]
	| WholenameRegex !String

//* See https://facebook.github.io/watchman/docs/expr/size.html.
:: IntConstraint
	= Eq !Int | Ne !Int
	| Gt !Int | Ge !Int
	| Lt !Int | Le !Int

/**
 * See https://facebook.github.io/watchman/docs/expr/type.html.
 * This data type is not complete.
 */
:: FileType
	= Directory
	| RegularFile

:: Response =
	{ errors   :: ![String]
	, warnings :: ![String]
	, response :: !?ResponseR
	}

:: ResponseR
	= SubscribeResponse !SubscribeResponse
	| ChangeNotification !ChangeNotification

:: SubscribeResponse =
	{ subscribe :: !String
	, clock     :: !String
	}

:: ChangeNotification =
	{ clock        :: !String
	, files        :: ![FilePath]
	, root         :: !FilePath
	, subscription :: !String
	}

/**
 * @param The directory to watch.
 * @param An expression for the files to watch.
 */
watchDirectory :: !FilePath !Expression !*World -> (!MaybeError String Watchman, !*World)

appendOutput :: !String !Watchman -> MaybeError String Watchman
