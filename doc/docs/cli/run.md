---
vim: noexpandtab tabstop=2 shiftwidth=2
---

# `nitrile run`

This command is used to run executables distributed in the `bin` directory of
packages. The package containing the executable may be installed both locally
(in a project scope) and globally (see [`nitrile global`](global.md)).

`nitrile run` will execute the executable for the latest package that is
compatible with the dependencies of the current project. `nitrile run` can also
be used outside projects; in this case, the latest version is selected.

!!! info
	Currently, when an executable is run from a package that has dependencies,
	Nitrile will check that the dependencies can be resolved. However, the
	program has no way of accessing the dependencies.

## Usage

`nitrile run` accepts the following positional arguments.
[Global options](global-options.md) must be specified before `run`:

```sh
nitrile [OPTS] run PACKAGE EXECUTABLE [ARGS]
```

- `PACKAGE` is the package in which the executable to execute is located.
- `EXECUTABLE` is the executable to execute.
- `ARGS` are the arguments to pass on to the executable.

## Shorthands

When a package is installed globally with
[`nitrile global install`](global.md#nitrile-global-install-package-version), a
script is created in the `bin` directory of the global Nitrile installation for
each executable in the package. This script calls `nitrile run` under the hood
to let Nitrile select an appropriate version of the package and run the
executable. Such a script has the same name as the executable and, on POSIX,
looks somewhat like:

```sh
#!/bin/sh
nitrile run PACKAGE EXECUTABLE $@
```

Therefore, in most cases you do not have to run `nitrile run` yourself, but can
simply call `EXECUTABLE`.
