---
vim: noexpandtab tabstop=2 shiftwidth=2
---

# Installing Nitrile

To install Nitrile for the first time, follow the
[instructions on the Clean website](https://clean-lang.org/about.html#install).

To build Nitrile from source, you need either a working Nitrile installation or
a working [Clean][] installation. You can find the source code on
[GitLab](https://gitlab.com/clean-and-itasks/nitrile).

## Updating Nitrile

To update your Nitrile version, [`nitrile global`](../cli/global.md) can be
used. For instance, to update from version 0.5 to 0.6, use the following:

```txt
$ nitrile global install nitrile 0.6
$ nitrile global remove nitrile 0.5
```

When multiple versions are installed, the latest is used automatically when you
run `nitrile`. It is currently not possible to run an older version.

## Tooling

You may also want to set up your editor with one of the
[plugins](https://clean-lang.org/about.html#tooling).

[Clean]: https://clean.cs.ru.nl
